﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Custom_Deserialisation_Prototype
{
    public class Root
    {
        public int id { get; set; }
        public Child child { get; set; }
    }

    //[Serializable]
    public class Child //: ISerializable
    {

        public dynamic field1 { get; set; }
        public string field2 { get; set; }
        public string Customer { get; set; }
        public List<string> Assets { get; set; }

        public Child()
        {
            Assets = new List<string>();
        }
    }
}