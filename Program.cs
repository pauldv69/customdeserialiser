﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Custom_Deserialisation_Prototype
{
    class Program
    {
        static void Main(string[] args)
        {

            string content = @"{
							""id"": 1,  
							""child"":   {
								""field1"": ""testing"",
								""field2"": ""another string"",
								""CustomFieldCustomer"": ""Customer Name"",
								""CustomFieldAssets"": [""asset1"", ""asset2"", ""asset3""]
								}
							}";

            var contractResolver = new FieldMappingContractResolver
            {
                FieldMappings = new Dictionary<string, string>()
                    {{"Assets", "CustomFieldAssets"}, {"Customer", "CustomFieldCustomer"}}
            };
            var result = (Root)JsonConvert.DeserializeObject(content, typeof(Root), new JsonSerializerSettings { ContractResolver = contractResolver });

            Console.WriteLine($"id: {result.id}");
            Console.WriteLine($"child.field2: {result.child.field2}");
            Console.WriteLine($"child.field1: {result.child.field1}");
            Console.WriteLine($"child.Customer: {result.child.Customer}");
            Console.WriteLine($"child.Assets.Count: {result.child.Assets.Count}");
            foreach (var childAsset in result.child.Assets)
            {
                Console.WriteLine($"    Asset: {childAsset}");
            }

            Console.ReadKey();

        }
    }

    public class FieldMappingContractResolver : DefaultContractResolver
    {
        public Dictionary<string, string> FieldMappings;
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);
            if (FieldMappings.ContainsKey(property.PropertyName))
                property.PropertyName = FieldMappings[property.PropertyName];
            return property;
        }
    }
}
